#include <stdio.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdbool.h>
#include "mpi.h"



#include "lu.h"

bool is_master();

void mat_lu(matrix_t *A) {
    int i, j, k;
    int N = (int) A->size;
    MPI_Status mpi_status;

    int column_per_node = N / (world_size - 1);

    if(is_master()) printf("[%s][%d] column_per_node=%d\n", node_name, node_rank, column_per_node);
    int maps[N]; // who sent to who during the broadcast
    for (j = 0; j < N; j++) maps[j] = 1 + j % (world_size - 1);

    MPI_Barrier(MPI_COMM_WORLD);
    // Decomposition itself
    for (i = 0; i < N; i++) {
        int len_of_row_k = N - i - 1;
        for (j = i + 1; j < N; j++) {
            if (node_rank == 0) { // Master will receive columns from all workers
                MPI_Recv(&(A->m[j][i]), len_of_row_k + 1, MPI_DOUBLE, maps[j], MPI_ANY_TAG, MPI_COMM_WORLD, &mpi_status);
                printf("[master] Receive line ");
            } else if (node_rank == maps[j]) { // Worker compute line and then send it to master
                A->m[j][i] = A->m[j][i] / A->m[i][i];
                for (k = i + 1; k < N; k++) {
                    A->m[j][k] -= A->m[j][i] * A->m[i][k];
                }

                printf("[%s][%d] Sending m[j=%d][i=%d] of length %d from %d to master\n", node_name, node_rank, j, i, len_of_row_k + 1, maps[j]);
                MPI_Send(&(A->m[j][i]), len_of_row_k + 1, MPI_DOUBLE, 0, 1515, MPI_COMM_WORLD);
            }
        }

        if(is_master()) printf("[master][%d-%d] len_of_row_k² = %d\n", node_rank, i, len_of_row_k * len_of_row_k);
        if(len_of_row_k * len_of_row_k) { //Broadcast only if there is something to broadcast
            MPI_Bcast(A->m[i] + i, len_of_row_k * len_of_row_k, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        }
        MPI_Barrier(MPI_COMM_WORLD);
    }
}

#define TIME_DIFF(t1, t2) (((t2.tv_sec - t1.tv_sec)*1e6) + (t2.tv_usec - t1.tv_usec))

int main(int argc, char **argv) {

    /* Install our signal handler *
    struct sigaction sa;

    sa.sa_handler = (void *)bt_sighandler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;

    sigaction(SIGSEGV, &sa, NULL);
    sigaction(SIGUSR1, &sa, NULL);
    /* End signal handler */

    if (argc < 2) {
        printf("Usage: %s matrix_size\n", argv[0]);
        return EXIT_FAILURE;
    }

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &node_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Get_processor_name(node_name, &name_len);

    size_t size = (size_t) atoi(argv[1]);
    if (is_master()) printf("Computing LU factorization (matrix size = %lu, %d nodes)...\n", size, world_size);

    srand(0);

    matrix_t *A = allocate_matrix(size);
    fill_matrix(A);

#if (defined(DEBUG) || defined(SHOW_MATRIX))
    matrix_t *A_backup = allocate_matrix(size);
    copy_mat(A, A_backup);
#endif

    struct timeval t1, t2;

    gettimeofday(&t1, NULL);
    mat_lu(A);
    gettimeofday(&t2, NULL);
    if (is_master()) printf("\tdone !\n");

    double duration = TIME_DIFF(t1, t2);
    if (is_master()) printf(" Computation took %lf ms\n", duration / 1000);

#if SHOW_MATRIX
    if (size < 10 && is_master()) {
        printf("\n\nResultat:\n");
        print_result(A, A_backup);
    }
#endif

#if DEBUG
    if (is_master()) check_result(A, A_backup);
    free_matrix(A_backup);
#endif

    MPI_Finalize();
    free_matrix(A);
    return EXIT_SUCCESS;
}

bool is_master() {
    return node_rank == 0;
}