set xlabel 'iteration'
set ylabel 'duree(ms)'

set xrange [:10000]

plot [*:*] [*:*] './omp_2mpi.txt' title 'OpenMP' with linespoints
replot './omp_2mpi.txt' using 1:3 title 'MPI (mémoire)' with linespoints
pause -1 'Taper Enter pour quitter'
