set surface 
set contour surface
set dgrid3d
set style data lines

set clabel '%8.2f'
set key right
set title "MPI Broadcast"
set xlabel "Matrix size"
set ylabel "Process number"
set zlabel "Time (ms)"

set term png
set output "broadcast.png"
splot "broadcast.txt" using 1:2:3 notitle
