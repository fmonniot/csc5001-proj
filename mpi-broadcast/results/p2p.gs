#set data style lines
set surface 
set contour surface
set dgrid3d
set style data lines

#set view 60, 30, 1, 1
set clabel '%8.2f'
set key right
set title "MPI Point-to-Point"
set xlabel "Matrix size"
set ylabel "Process number"
set zlabel "Time (ms)"

set term png
set output "p2p.png"
splot "p2p.txt" using 1:2:3 notitle
