set xlabel 'iteration'
set ylabel 'duree(ms)'
plot [*:*] [*:*] './omp_mpi.txt' title 'OpenMP' with linespoints
replot './omp_mpi.txt' using 1:3 title 'MPI (4 processus)' with linespoints
pause -1 'Taper Enter pour quitter'
