#ifndef LU_H
#define LU_H

#include <stdio.h>

/* Uncomment to enable debugging */
#define DEBUG 1

/* Uncomment to print the resulting matrices */
#define SHOW_MATRIX 1

typedef struct {
    double **m;
    size_t size;
} matrix_t;

/* allocate a size*size matrix */
matrix_t *allocate_matrix(size_t size);

/* fill a size*size matrix with (more or less) random values.
 * We garanty that there is an LU decomposition of the resulting matrix
 */
void generate_random_matrix(matrix_t *mat);

/* fill a size*size matrix with zeroes */
void zero_mat(matrix_t *mat);

/* copy matrix A into B */
void copy_mat(matrix_t *A, matrix_t *B);

/* free a matrix */
void free_matrix(matrix_t *m);

#define _QUOT(x) #x
#define QUOTE(x) _QUOT(x)

/* Print matrix a */
#define show_mat(a) do { printf(QUOTE(a)" =\n"); print_matrix(a); } while(0)

void print_matrix(matrix_t *mat);

/* print the L part of a matrix */
void print_L(matrix_t *mat);

/* print the U part of a matrix */
void print_U(matrix_t *mat);

/* print the results  */
void print_result(matrix_t *mat, matrix_t *mat_backup);

/* perform a matrix multiplication (C = A*B) */
void matmul(matrix_t *A, matrix_t *B, matrix_t *C);

/* fill matrix A */
void fill_matrix(matrix_t *A);

/* extract the L and U parts of A into separate matrixes */
void extract_LU(matrix_t *A, matrix_t *L, matrix_t *U);

/* Check the result of the LU factorization */
void check_result(matrix_t *A_computed, matrix_t *A_expected);

#endif	/* LU_H */