#include <stdio.h>
#include <sys/time.h>
#include <stdlib.h>
#include <omp.h>

#include "lu.h"

void mat_lu(matrix_t *A) {
    int i, j, k;
    size_t N = A->size;

    for (i = 0; i < N; i++) {

#pragma omp parallel for private(j) shared(i)
        for (j = i + 1; j < N; j++) {
            A->m[j][i] = A->m[j][i] / A->m[i][i];
        }

#pragma omp parallel for private(k) shared(i,j) schedule(static)
        for (j = i + 1; j < N; j++) {
            for (k = i + 1; k < N; k++) {
                A->m[j][k] = A->m[j][k] - A->m[j][i] * A->m[i][k];
            }
        }
    }
}

#define TIME_DIFF(t1, t2) (((t2.tv_sec - t1.tv_sec)*1e6) + (t2.tv_usec - t1.tv_usec))

int main(int argc, char **argv) {

    if (argc < 2) {
        printf("Usage: %s matrix_size\n", argv[0]);
        return EXIT_FAILURE;
    }

    size_t size = (size_t) atoi(argv[1]);

    srand(0);

    matrix_t *A = allocate_matrix(size);
    fill_matrix(A);

#if (defined(DEBUG) || defined(SHOW_MATRIX))
    matrix_t *A_backup = allocate_matrix(size);
    copy_mat(A, A_backup);
#endif

    printf("Computing LU factorization (matrix size = %lu, %d threads)...\n", size, omp_get_max_threads());
    struct timeval t1, t2;

    gettimeofday(&t1, NULL);
    mat_lu(A);
    gettimeofday(&t2, NULL);
    printf("\tdone !\n");

    double duration = TIME_DIFF(t1, t2);
    printf(" Computation took %lf ms\n", duration / 1000);

#if SHOW_MATRIX
    if(size < 10) {
        printf("\n\nResultat:\n");
        print_result(A, A_backup);
    }
#endif

#if DEBUG
    check_result(A, A_backup);
    free_matrix(A_backup);
#endif

    free_matrix(A);
    return 0;
}