#include <stdio.h>
#include <stdlib.h>

#include "lu.h"

void free_matrix(matrix_t *m) {
    free(m->m[0]);
    free(m->m);
    free(m);
}

matrix_t *allocate_matrix(size_t size) {
    matrix_t *res = (matrix_t*) malloc(sizeof(matrix_t));
    res->size = size;
    // pointer allocation to rows
    res->m = (double**) malloc(sizeof(double *) * size);
    // allocate rows and set pointers continuously
    res->m[0] = (double *) malloc((size_t)((size*size)*sizeof(double)));
    int i;
    for(i=1; i<=size; i++){
        res->m[i]=res->m[i-1] + size;
    }
    return res;
}

void generate_random_matrix(matrix_t *mat) {
    int i, j;
    int maxval = 20;

    for (i = 0; i < mat->size; i++) {
        for (j = 0; j < mat->size; j++) {
            int val;

            do {
                /* random value comprise between -maxval and +maxval
                 * if i==j, val should be != 0 so that the matrix can be decomposed
                 */
                val = ((rand()) % (2 * maxval)) - maxval;
            } while (i == j && val == 0);

            mat->m[i][j] = val;
        }
    }
}

void zero_mat(matrix_t *mat) {
    int i, j;
    for (i = 0; i < mat->size; i++) {
        for (j = 0; j < mat->size; j++) {
            mat->m[i][j] = 0;
        }
    }
}

/* copy A into B */
void copy_mat(matrix_t *A, matrix_t *B) {
    int i, j;
    for (i = 0; i < A->size; i++) {
        for (j = 0; j < A->size; j++) {
            B->m[i][j] = A->m[i][j];
        }
    }
}


void print_matrix(matrix_t *mat) {
    int i, j;
    for (i = 0; i < mat->size; i++) {
        for (j = 0; j < mat->size; j++) {
            printf("|  %lf  | ", mat->m[i][j]);
        }
        printf("\n");
    }
}

void print_L(matrix_t *mat) {
    int i, j;
    printf("L = \n");
    for (i = 0; i < mat->size; i++) {
        for (j = 0; j < i; j++) {
            printf("|  %lf  | ", mat->m[i][j]);
        }
        printf("|  %lf  | ", 1.);

        for (j = i + 1; j < mat->size; j++) {
            printf("|  %lf  | ", 0.);
        }
        printf("\n");
    }
}

void print_U(matrix_t *mat) {
    int i, j;
    printf("U = \n");
    for (i = 0; i < mat->size; i++) {
        for (j = 0; j < i; j++) {
            printf("|  %lf  | ", 0.);
        }

        for (j = i; j < mat->size; j++) {
            printf("|  %lf  | ", mat->m[i][j]);
        }
        printf("\n");
    }
}

void print_result(matrix_t *mat,
        matrix_t *mat_backup) {
    printf("A = \n");
    print_matrix(mat_backup);

    print_L(mat);
    print_U(mat);
}

/* compute C = A * B (where A, B and C are size*size matrixes  */
void matmul(matrix_t *A,
        matrix_t *B,
        matrix_t *C) {
    int i, j, k;
    for (i = 0; i < A->size; i++) {
        for (j = 0; j < A->size; j++) {
            C->m[i][j] = 0;
            for (k = 0; k < A->size; k++) {
                C->m[i][j] += A->m[i][k] * B->m[k][j];
            }
        }
    }
}

void fill_matrix(matrix_t *A) {
#if 0
      assert(A->size == 3);
      /* Exemple de matrice 3x3:
               A      =      L     .     U
          | 6  0  2 |   | 1  0  0 | | 6  0  2 |
          |24  1  8 | = | 4  1  0 | | 0  1  0 |
          |-12 1 -3 |   |-2  1  1 | | 0  0  1 |
       */
      double A3[3][3] = {{6,0,2},{24,1,8},{-12, 1, -3}};
      int i,j;

      for(i=0;i<A->size; i++) {
        for(j=0; j<A->size; j++) {
          A->m[i][j]=A3[i][j];
        }
      }
    #else
    generate_random_matrix(A);
#endif
    return;
}

void extract_LU(matrix_t *A,
        matrix_t *L,
        matrix_t *U) {
    int i, j;
    for (i = 0; i < A->size; i++) {
        for (j = 0; j < i; j++) {
            L->m[i][j] = A->m[i][j];
        }
        L->m[i][i] = 1;

        for (j = i; j < A->size; j++) {
            U->m[i][j] = A->m[i][j];
        }
    }
}

#define EPSILON 1e-8

void check_result(matrix_t *A_computed,
        matrix_t *A_expected) {
    printf("Checking results...\n");
    int i, j, k;
    int error = 0;

#ifdef SHOW_MATRIX
    matrix_t *C = allocate_matrix(A_computed->size);
#endif

#define GET_U(_i, _j) (((_j)>=(_i)) ? A_computed->m[(_i)][(_j)] : 0)
#define GET_L(_i, _j) (((_j)<(_i)) ? A_computed->m[(_i)][(_j)] : ((_i)==(_j) ? 1: 0))

    for (i = 0; i < A_computed->size; i++) {
        for (j = 0; j < A_computed->size; j++) {
            double A_ij = 0;
            for (k = 0; k < A_computed->size; k++) {
                A_ij += GET_L(i, k) * GET_U(k, j);
            }

#ifdef SHOW_MATRIX
            C->m[i][j] = A_ij;
#endif
            double v = A_ij - A_expected->m[i][j];

            if (v < 0)
                v = -v;

            if (v > EPSILON) {
                printf("Computed[%d][%d] = %lg (should be %lg). residual= %g\n", i, j, A_ij, A_expected->m[i][j], v);
                error = 1;
            }
        }
    }

    if (error) {
        printf("\tCheck result: failed!\n");
#ifdef SHOW_MATRIX
        print_matrix(C);
#endif
    } else {
        printf("\tCheck result: success!\n");
    }
}