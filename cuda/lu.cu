#include <stdio.h>
#include <sys/time.h>
#include <stdlib.h>
#include "lu.h"

#include <cuda.h>
#include <cuda_runtime.h>
#include <helper_cuda.h>
#include <cublas_v2.h>

__global__ void mat_gpu(double* m, int N, int i) {
    int idx = blockDim.x*blockIdx.x + threadIdx.x; // k in sequential
    int idy = blockDim.y*blockIdx.y + threadIdx.y; // j in sequential

    if(idx < N && idy < N && idx >= i + 1 && idy >= i + 1) {
        m[N*idy + idx] -= m[N*idy + i] * m[N*i + idx];
    }
}

__global__ void reduce_gpu(double* m, int N, int i) {
  int numerobloc = blockIdx.x +  gridDim.x * blockIdx.y;
  int idx = numerobloc*1024 + 32*threadIdx.y + threadIdx.x;

  if(idx >= i + 1 && idx < N) {
    m[N*idx + i] /= m[N*i + i];
  }
}

void mat_lu(matrix_t *A) {
    int i;
    int N = (int) A->size;

    double* m_d;
    int nbblocks = 1 + (N*N) / (32*32);
    printf("nbblocks=%d, nbthreadbyblock=32x32\n", nbblocks);

    dim3 nbthreadbyblock(32,32,1);
    dim3 nbblockbygrid(nbblocks,nbblocks,1);

    // Allocate & transfer on device
    checkCudaErrors(cudaMalloc((void**) &m_d, sizeof(double) * N * N));
    checkCudaErrors(cudaMemcpy(m_d, A->m[0], sizeof(double) * N * N, cudaMemcpyHostToDevice));

    for (i = 0; i < N; i++) {
        if(N - i - 1 > 0) {
            reduce_gpu<<<nbblockbygrid,nbthreadbyblock>>>(m_d, N, i);
            mat_gpu<<<nbblockbygrid,nbthreadbyblock>>>(m_d, N, i);
        }
    }

    checkCudaErrors(cudaMemcpy(A->m[0], m_d, sizeof(double) * N * N, cudaMemcpyDeviceToHost) );
}

#define TIME_DIFF(t1, t2) (((t2.tv_sec - t1.tv_sec)*1e6) + (t2.tv_usec - t1.tv_usec))

int main(int argc, char **argv) {

    if (argc < 2) {
        printf("Usage: %s matrix_size\n", argv[0]);
        return EXIT_FAILURE;
    }

    size_t size = (size_t) atoi(argv[1]);

    srand(0);

    matrix_t *A = allocate_matrix(size);
    fill_matrix(A);

#if (defined(DEBUG) || defined(SHOW_MATRIX))
    matrix_t *A_backup = allocate_matrix(size);
    copy_mat(A, A_backup);
#endif

    printf("Computing LU factorization (matrix size = %lu)...\n", size);
    struct timeval t1, t2;

    gettimeofday(&t1, NULL);
    mat_lu(A);
    gettimeofday(&t2, NULL);
    printf("\tdone !\n");

    double duration = TIME_DIFF(t1, t2);
    printf(" Computation took %lf ms\n", duration / 1000);

#if SHOW_MATRIX
    if (size < 10) {
        printf("\n\nResultat:\n");
        print_result(A, A_backup);
    }
#endif

#if DEBUG
    check_result(A, A_backup);
    free_matrix(A_backup);
#endif

    free_matrix(A);
    return 0;
}
