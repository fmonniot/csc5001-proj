set xlabel 'iteration'
set ylabel 'duree(ms)'
plot [*:*] [*:*] './seq_vs_cuda.dat' title 'sequential' with linespoints
replot './seq_vs_cuda.dat' using 1:3 title 'cuda' with linespoints
pause -1 'Taper Enter pour quitter'
