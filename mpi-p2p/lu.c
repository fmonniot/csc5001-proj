#include <stdio.h>
#include <stdlib.h> 
#include <sys/time.h>
#include "lu.h"
#include "mpi.h"

#define TIME_DIFF(t1, t2) (((t2.tv_sec - t1.tv_sec)*1e6) + (t2.tv_usec - t1.tv_usec))

// MPI node state (global per processus)
int myrank, world_size;

void master(matrix_t *A);

void slave(matrix_t *pT);

double **allocMatrix(int N);

double *allocRow(int N);

MPI_Datatype *create_datatype(int N, int *integer, double *array);

int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("Usage: %s matrix_size\n", argv[0]);
        return EXIT_FAILURE;
    }

    int size = atoi(argv[1]);
    srand(0);

    matrix_t *A = allocate_matrix((size_t) size);
    fill_matrix(A);

#if (defined(DEBUG) || defined(SHOW_MATRIX))
    matrix_t *A_backup = allocate_matrix((size_t) size);
    copy_mat(A, A_backup);
#endif

    struct timeval t1, t2;

    gettimeofday(&t1, NULL);

    MPI_Init(&argc, (char ***) &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    if (myrank != 0) {
        slave(A);
    }

    if (myrank == 0) {
        master(A);

        gettimeofday(&t2, NULL);
        printf("\tdone !\n");
        double duration = TIME_DIFF(t1, t2);
        printf(" Computation took %lf ms\n", duration / 1000);
#if SHOW_MATRIX
        if (size < 10) {
            print_matrix(A);
            print_L(A);
            print_U(A);
        }
#endif

#if DEBUG
        check_result(A, A_backup);
        free_matrix(A_backup);
#endif
    }

    if (myrank == 0) {
    }
    MPI_Finalize();

    free_matrix(A);
    return 0;
}

void slave(matrix_t *A) {
    int N = (int) A->size;
    double **matt = NULL;
    double *tab = NULL;
    int i, j, k;

    int nbrEPCase = N / (world_size - 1);
    int nbrEDernCase = nbrEPCase + (N % (world_size - 1));

    int pivot;
    MPI_Status status;

    for (pivot = 0; pivot < N; pivot++) {
        tab = (double *) malloc(N * sizeof(double));

        // Receive our data
        MPI_Recv(tab, 1, *create_datatype(N, &pivot, tab), 0, 99, MPI_COMM_WORLD, &status);

        //Traitement d'une partie
        {
            int begin_interval, end_interval, count;

            if (myrank == (world_size - 1)) { // last element
                begin_interval = (myrank * nbrEPCase) - nbrEPCase;
                end_interval = N - 1;

                count = nbrEDernCase;
            } else { // intermediate element
                begin_interval = (myrank * nbrEPCase) - nbrEPCase;
                end_interval = (myrank * nbrEPCase) - 1;

                count = nbrEPCase;
            }

            //modification du tableau
            double *tabR = allocRow(count);
            for (i = begin_interval, j = 0; i <= end_interval; i++, j++) {
                if (i <= pivot) tabR[j] = tab[i];
                else tabR[j] = tab[i] / tab[pivot];
            }

            MPI_Send(tabR, count, MPI_DOUBLE_PRECISION, 0, 99, MPI_COMM_WORLD);
            free(tabR);
        }

        // TODO Receive the matrix in one go (need to allocate the matrix before receiving it)
        matt = allocMatrix(N);
        for (i = 0; i < N; i++) {
            MPI_Recv((matt[i]), N, MPI_DOUBLE_PRECISION, 0, 99, MPI_COMM_WORLD, &status);
        }

        //traitement de la sous matrice, chacun avec ses lignes associées
        double *tabR = allocRow(nbrEPCase);
        for (i = myrank - 1; i < N; i += (world_size - 1)) {
            tabR = matt[i];
            if (i > pivot) {
                for (k = pivot + 1; k < N; k++) {
                    tabR[k] -= tabR[pivot] * matt[pivot][k]; // Reduction
                }
            }
            MPI_Send(tabR, N, MPI_DOUBLE_PRECISION, 0, 99, MPI_COMM_WORLD);
        }
        free(tabR);
        free(tab);
        free(matt);
    }
}

void master(matrix_t *A) {
    double **mat = A->m;
    int N = (int) A->size;
    int tailleTabRecu = 0;
    int iProc, i, j, pivot;
    double *tab = NULL;

    MPI_Status status;
    int count;

    double *tab1 = NULL;
    double *tabG = NULL;

    for (pivot = 0; pivot < N; pivot++) {
        tab = allocRow(N);

        // Prepare the array that will be sent
        for (i = 0; i < N; i++) {
            tab[i] = mat[i][pivot];
        }

        // Send the array to others process
        {
            MPI_Datatype *message_type = create_datatype(N, &pivot, tab);

            for (iProc = 1; iProc <= world_size - 1; iProc++) {
                MPI_Send(tab, 1, *message_type, iProc, 99, MPI_COMM_WORLD);
            }

            free(message_type);
        }

        // Receive array chunk from others process
        tailleTabRecu = 0;
        for (iProc = 1; iProc <= world_size - 1; iProc++) {
            MPI_Probe(iProc, 99, MPI_COMM_WORLD, &status);

            MPI_Get_count(&status, MPI_DOUBLE_PRECISION, &count);

            tab1 = (double *) malloc(count * sizeof(double));
            MPI_Recv(tab1, count, MPI_DOUBLE_PRECISION, iProc, 99, MPI_COMM_WORLD, &status);

            tailleTabRecu = tailleTabRecu + count;

            tabG = (double *) realloc(tabG, tailleTabRecu * sizeof(double));

            if (iProc == world_size - 1) { // This come from the last process
                for (i = (tailleTabRecu - count), j = 0; i < tailleTabRecu; i++, j++) {
                    tabG[i] = tab1[j];
                }
            } else { // This come from an intermediate process
                for (i = (iProc * count) - count, j = 0; i <= (iProc * count) - 1; i++, j++) {
                    tabG[i] = tab1[j];
                }
            }
        }

        // TODO ??
        for (i = 0; i < N; i++) mat[i][pivot] = tabG[i];

        //envoi de la matrice aux autres
        for (iProc = 1; iProc <= world_size - 1; iProc++) {
            // TODO send this matrix with only one communication
            for (i = 0; i < N; i++) {
                MPI_Send(mat[i], N, MPI_DOUBLE_PRECISION, iProc, 99, MPI_COMM_WORLD);
            }
        }

        // Receive matrix
        for (iProc = 1; iProc <= world_size - 1; iProc++) {
            for (i = iProc - 1; i < N; i = i + (world_size - 1)) {
                MPI_Recv(mat[i], N, MPI_DOUBLE_PRECISION, iProc, 99, MPI_COMM_WORLD, &status);
            }
        }
    }

    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            A->m[i][j] = mat[i][j];
        }
    }
}

MPI_Datatype *create_datatype(int N, int *integer, double *array){
    MPI_Aint array_of_displacements[2];
    MPI_Aint a1, a2;
    MPI_Datatype type[2] = {MPI_DOUBLE_PRECISION, MPI_INTEGER};
    int block_lengths[2] = {N, 1};

    MPI_Get_address(array, &a1);
    MPI_Get_address(integer, &a2);
    array_of_displacements[0] = 0;
    array_of_displacements[1] = a2 - a1;

    MPI_Datatype *datatype = malloc(sizeof(MPI_Datatype));
    MPI_Type_create_struct(2, block_lengths, array_of_displacements, type, datatype);
    MPI_Type_commit(datatype);

    return datatype;
}

double *allocRow(int N) {
    return (double *) malloc(N * sizeof(double));
}

double **allocMatrix(int N) {
    int i;
    double **mat2 = (double **) malloc(N * sizeof(double *));
    for (i = 0; i < N; i++) {
        mat2[i] = allocRow(N);
    }
    return mat2;
}

