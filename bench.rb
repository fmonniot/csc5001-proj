#usr/bin/env ruby
require 'set'

class DataSet
  def initialize
    @dataset = {}
  end

  def add_result(group, id, value)
    @dataset[group] = {} unless @dataset[group]
    @dataset[group][id] = value unless @dataset[group][id]
  end

  def each
    @dataset.each do |k, group|
      yield k, group
    end
  end
end

class Bench
  def initialize(commands=Set.new, args=Set.new)
    @dataset = DataSet.new
    @commands = commands
    @variants = args
  end

  def add_command(command)
    @commands.add command
  end

  def add_variant(variant)
    @variants.add variant
  end

  def perform
    total = @commands.length * @variants.length
    puts "Launching #{total} executions"
    @commands.each do |cmd|
      @variants.each do |arg|
        out = `#{cmd} #{arg}`.scan(/[\d\.]+/)
        @dataset.add_result arg, cmd, out[3].to_f
        putc '.'
      end
    end
  end

  def print
    @dataset.each do |iter, group|
      p iter.to_s + ' ' + group.sort.map { |a| a[1] }.join(' ')
    end
  end

  def save(file_name)
    File.open(file_name+'.txt', 'w') do |file|
      file.write "set xlabel 'iteration'\n"
      file.write "set ylabel 'duree(ms)'\n"
      @commands.each_with_index do |cmd, i|
        if i == 0
          file.write "plot [*:*] [*:*] '#{file_name}.dat' title '#{cmd}' with linespoints\n"
        else
          file.write "replot '#{file_name}.dat' using 1:#{i+1} title '#{cmd}' with linespoints\n"
        end
      end
      file.write "pause -1 'Taper Enter pour quitter'\n"
    end
    File.open(file_name+'.dat', 'w') do |file|
      @dataset.each do |iter, group|
        line = iter.to_s + ' ' + group.sort.map { |a| a[1] }.join(' ') + "\n"
        file.write line
      end
    end
  end
end

b = Bench.new
b.add_command 'bin/lu_seq'
b.add_command 'bin/lu_par'
b.add_command 'bin/lu_par_coll'
b.add_command 'OMP_SCHEDULE= "dynamic, N/10" bin/lu_par_coll'
b.add_command 'OMP_SCHEDULE= "dynamic, N/4" bin/lu_par_coll'
b.add_command 'OMP_SCHEDULE= "guided, N/10" bin/lu_par_coll'
b.add_command 'OMP_SCHEDULE= "guided, N/4" bin/lu_par_coll'
b.add_command 'OMP_SCHEDULE= "static, N/10" bin/lu_par_coll'
b.add_command 'OMP_SCHEDULE= "static, N/4" bin/lu_par_coll'

b.add_variant 1000
b.add_variant 2000
b.add_variant 3000
b.add_variant 4000
b.add_variant 5000

b.perform

b.print
b.save './bench_gnu'
